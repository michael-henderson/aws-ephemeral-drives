#!/bin/bash

## set error output color to red
RED='\033[0;31m'
NC='\033[0m' # No Color

function error() {
  echo -e "${RED}ERROR: $1${NC}"
  exit 1
}

## Script needs to be ran by root to create volume groups
if [[ $EUID -ne 0 ]]; then
   error "This script must be run as root!"
fi

version="1.0.0"

# location of this script.
scriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Default option values
exitScript=0
debug=0
printLog=0
fileSystem="ext4"
volGroup="VolGroup02"
logVol="LogVol02"
mountOptions="defaults,nofail"
args=()

# Log is only used when the '-l' flag is set.
logFile="/var/log/aws-ephemeral-drives.log"

function mainScript() {

  numEphDrives=`curl http://169.254.169.254/latest/meta-data/block-device-mapping/ |grep -c 'ephemeral'`
  echo "Total of $numEphDrives ephemeral drives found from ec2metadata."
  echo "List of drives found with lsblk:"
  ephDriveNames=( $(lsblk -o NAME -nd |sed ':a;N;$!ba;s/\\n/\\t/g' |sed 's/^/\/dev\//' |grep -v loop |tail -n +2) )
  for i in "${ephDriveNames[@]}"
  do
    echo $i
  done

  if [ $numEphDrives != ${#ephDriveNames[@]} ]; then
    error "ec2metadata found a different number of ephemeral drives than lsblk."
  fi

  ## AWS adds an entry for the first ephemeral device that attempts to mount it to /mnt. We need to remove that entry now that
  ## we are including it in the logical volume. The -i.bak option to sed will create a backup copy of the original fstab at /etc/fstab.bak
  mntDevice=( $(echo ${ephDriveNames[0]} |sed 's/\/dev\///') )
  echo "Removing default ephemeral entry from /etc/fstab.  Creating backup copy of original at /etc/fstab.bak"
  sed -i.bak "/$mntDevice/d" /etc/fstab

  echo "Creating ${volGroup} Volume Group with ${ephDriveNames[@]}..."
  /sbin/vgcreate ${volGroup} $(echo ${ephDriveNames[@]}) || error "vgcreate failed!"
  echo "Creating Logical Volume /dev/${volGroup}/${logVol}..."
  /sbin/lvcreate -l 100%VG -n ${logVol} ${volGroup} || error "lvcreate failed!"
  echo "Formatting /dev/${volGroup}/${logVol} with ${fileSystem} filesystem. May take a few moments depending on the filesystem size..."
  /sbin/mkfs.${fileSystem} /dev/${volGroup}/${logVol} || error "mkfs.${fileSystem} failed!"
  echo "Making directory ${dirName} to mount /dev/${volGroup}/${logVol} to..."
  /bin/mkdir -p ${dirName} || error "mkdir failed!"
  echo "Adding /dev/${volGroup}/${logVol}   ${dirName} to /etc/fstab"
  echo "/dev/${volGroup}/${logVol}       ${dirName}    ${fileSystem}    ${mountOptions},comment=aws-ephemeral-drives       0       0" >> /etc/fstab
  echo "Mounting /dev/${volGroup}/${logVol} to ${dirName}..."
  /bin/mount ${dirName}|| error "Failed to mount!"
}

# Print usage
usage() {
  echo -n "USAGE: ./aws-ephemeral-drives.sh -d [directory name] [OPTIONS]

This script finds ephemeral block devices inside AWS instances, creates a Volume Group and Logical Volume from them,
formats LogVol00 to the file system you choose and mounts it to the specified directory. --dirname is the only required option,
others have default values set.

 Options:
  -f, --filesystem  Filesystem to format drives with, ext3|ext4|xfs  (default: ext4)
  -g, --volgroup    Name of the Volume Group to be created (default: VolGroup02)
  -l, --logvol      Name of the Logical Volume to be created (default: LogVol02)
  -d, --dirname     Name of the directory to mount the created LogVol to. Use absolute path, e.g. /mnt/data
  -m, --mountops    Comma separated list of mount options to add to fstab entry (default: defaults,nofail)
  -p, --printlog    Print log to file to /var/log/aws-ephemeral-drives.log
  -d, --debug       Runs script in BASH debug mode (set -x)
  -h, --help        Display this help and exit
      --version     Output version information and exit
"
  exitScript=1
}

# Iterate over options breaking -ab into -a -b when needed and --foo=bar into
# --foo bar
optstring=h
unset options
while (($#)); do
  case $1 in
    # If option is of type -ab
    -[!-]?*)
      # Loop over each character starting with the second
      for ((i=1; i < ${#1}; i++)); do
        c=${1:i:1}

        # Add current char to options
        options+=("-$c")

        # If option takes a required argument, and it's not the last char make
        # the rest of the string its argument
        if [[ $optstring = *"$c:"* && ${1:i+1} ]]; then
          options+=("${1:i+1}")
          break
        fi
      done
      ;;

    # If option is of type --foo=bar
    --?*=*) options+=("${1%%=*}" "${1#*=}") ;;
    # add --endopts for --
    --) options+=(--endopts) ;;
    # Otherwise, nothing special
    *) options+=("$1") ;;
  esac
  shift
done
set -- "${options[@]}"
unset options

# Print help if no arguments were passed.
 [[ $# -eq 0 ]] && set -- "--help"

# Read the options and set stuff
while [[ $1 = -?* ]]; do
  case $1 in
    -f|--filesystem) shift; fileSystem=${1} ;;
    -g|--volgroup) shift; volGroup=${1} ;;
    -l|--logVol) shift; logVol=${1} ;;
    -d|--dirname) shift; dirName=${1} ;;
    -m|--mountops) shift; mountOptions=${1} ;;
    -p|--printlog) printLog=1 ;;
    -d|--debug) debug=1 ;;
    -h|--help) usage >&2; exitScript=1 ;;
    --version) echo "$(basename $0) ${version}"; exitScript=1 ;;
    --endopts) shift; break ;;
    *) die "invalid option: '$1'." ;;
  esac
  shift
done

# Store the remaining part as arguments.
args+=("$@")

# Exit on error. Append '||true' when you run the script if you expect an error.
set -o errexit

# Run in debug mode, if set
if [ "${debug}" == "1" ]; then
  set -x
fi

# Catch errors in a chain of pipes.
# e.g. if mysqldump fails in `mysqldump |gzip`
set -o pipefail

# Run unless help menu
if [ "${exitScript}" != "1" ]; then
  mainScript
fi
