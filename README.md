# aws-ephemeral-drives

This script finds ephemeral block devices inside AWS instances, creates a Volume Group (default: VolGroup02) and Logical Volume (default: LogVol02) from them, formats the logical volume to the file system you choose and mounts it to the specified directory. --dirname is the only required option, others have default values set.

USAGE: ./aws-ephemeral-drives.sh -d [directory name] [OPTIONS]
